// imports
const csv = require("csvtojson")
const fs = require("fs")
// output paths
const filePathMatches = "../data/matches.csv"
const outputPathMatches = "../data/matches.json"
const filePathDeliveries = "../data/deliveries.csv"
const outputPathDeliveries = "../data/deliveries.json"


// read matches.csv data and convert it to matches.json data

csv()
    .fromFile(filePathMatches)
    .then((jsonObj) => {
        fs.writeFileSync(outputPathMatches, JSON.stringify(jsonObj), (error) => {
            if (error) {
                console.log(error)
            }
        })
    })
console.log("matches.csv is converted to matches.json")

// read deliveries.csv data and convert it to deliveries.json data
csv()
    .fromFile(filePathDeliveries)
    .then((jsonObj) => {
        fs.writeFileSync(outputPathDeliveries, JSON.stringify(jsonObj), (error) => {
            if (error) {
                console.log(error)
            }
        })
    })


const iplData = require('./ipl')
// * Number of matches played per year for all the years in IPL.*

fs.writeFileSync('../public/output/matchesPerYear.json', JSON.stringify(iplData.filteredMatchesPerYear))

// * Number of matches won per team per year in IPL.

fs.writeFileSync('../public/output/matchesWonPerTeamPerYear.json', JSON.stringify(iplData.filteredMatchesWonPerYear))

// * extra runs conceded by each team

fs.writeFileSync('../public/output/extraRunConcededPerTeamPerYear.json', JSON.stringify(iplData.filteredExtraRunsConcededPerYear))

// * extra runs conceded by team in the year of 2015
fs.writeFileSync('../public/output/extraRunConcededPerTeamPerYear.json', JSON.stringify(iplData.filteredExtraRunsConcededPerYear))

// * top 10 economical bowler of year 2015
fs.writeFileSync('../public/output/top10EconomicalBowler2015.json', JSON.stringify(iplData.filteredTop10EconomicalBowlers2015))


