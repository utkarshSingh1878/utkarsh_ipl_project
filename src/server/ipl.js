// importing json data
const matchesJson = require('../data/matches.json')
const deliveriesJson = require('../data/deliveries.json')

// ! Q1. Number of matches played per year for all the years in IPL.
function filteredPlayedMatchesPerYear(matchesData) {
    let matchHashMap = {}
    matchesData.map((match) => {
        if (matchHashMap[match.season]) {
            matchHashMap[match.season] += 1
        } else {
            matchHashMap[match.season] = 1
        }
    })
    return matchHashMap
}
const filteredMatchesPerYear = filteredPlayedMatchesPerYear(matchesJson);

//! 2. Number of matches won per team per year in IPL.
function filterMatchesWonPerYear(matchData) {
    let hashMap1 = {}
    matchData.map((idx) => {
        if (!hashMap1[idx.season]) {
            hashMap1[idx.season] = {
            }
        }
    })
    Object.entries(hashMap1).map(entry => {
        let year = entry[0]
        // console.log(hashMap1[year])
        hashMap1[year] = collectTotalWins(matchData, year)
    })
    return hashMap1
}
function collectTotalWins(matchData, year) {
    let hashMap2 = {}
    matchData.map((idx) => {
        if (idx.season === year) {
            if (!hashMap2[idx.winner]) {
                hashMap2[idx.winner] = 1
            }
            else {
                hashMap2[idx.winner] += 1
            }
        }
    })
    return hashMap2
}
const filteredMatchesWonPerYear = filterMatchesWonPerYear(matchesJson)
// console.log(filteredMatchesWonPerYear)

//  !3. Extra runs conceded per team in the year 2016
function extraRunsConcededPerYear(deliveryData, matchesJson) {
    let matchesOf2016 = []
    let deliveriesOf2016 = []
    let extranRunPerTeam = {}
    matchesOf2016 = matchesJson.filter((obj) => {
        return obj.season === '2016'
    }).map(obj => obj.id)
    // console.log(matchesOf2016)
    deliveriesOf2016 = deliveryData.filter((obj) => {
        return matchesOf2016.includes(obj.match_id)
    }).map(obj => obj)
    // console.log(deliveriesOf2016)
    deliveriesOf2016.forEach(deliveryOf2016 => {
        if (extranRunPerTeam.hasOwnProperty(deliveryOf2016.bowling_team)) {
            extranRunPerTeam[deliveryOf2016.bowling_team] += parseInt(deliveryOf2016.extra_runs)
        }
        else {
            extranRunPerTeam[deliveryOf2016.bowling_team] = parseInt(deliveryOf2016.extra_runs)
        }
    })
    return extranRunPerTeam
}

const filteredExtraRunsConcededPerYear = extraRunsConcededPerYear(deliveriesJson, matchesJson)
// console.log(filteredExtraRunsConcededPerYear)


//! 4. Top 10 economical bowlers in the year 2015
function top10EconomicalBowlers2015(delivery, match) {
    let match2015 = [];
    let EconomicalBowlers2015 = {};

    match2015 = match.filter((obj) => {
        return obj.season === '2015'
    })

    match2015.forEach(matchObj => {
        delivery.forEach(deliveryObj => {
            //console.log(matchObj.id +" "+ deliveryObj.match_id)
            if (matchObj.id === deliveryObj.match_id) {
                if (EconomicalBowlers2015.hasOwnProperty(deliveryObj.bowler)) {

                    EconomicalBowlers2015[deliveryObj.bowler].economical += parseInt(deliveryObj.total_runs);
                    EconomicalBowlers2015[deliveryObj.bowler].delivery += 1;
                }
                else {

                    EconomicalBowlers2015[deliveryObj.bowler] = {};
                    EconomicalBowlers2015[deliveryObj.bowler].economical = +deliveryObj.total_runs;
                    EconomicalBowlers2015[deliveryObj.bowler].delivery = 1;
                }
            }
        })

    })
    let top10Economi = [];
    for (let key in EconomicalBowlers2015) {
        // console.log(EconomicalBowlers2015[key].delivery);
        let over;
        over = EconomicalBowlers2015[key].delivery / 6;
        EconomicalBowlers2015[key].economical /= over;
        top10Economi.push([key, EconomicalBowlers2015[key].economical])


    }
    top10Economi.sort(function (a, b) {
        return a[1] - b[1];
    })

    top10Economi = top10Economi.slice(0, 10);

    return top10Economi;
}

const filteredTop10EconomicalBowlers2015 = top10EconomicalBowlers2015(deliveriesJson, matchesJson)


module.exports = {
    filteredMatchesPerYear,
    filteredMatchesWonPerYear,
    filteredExtraRunsConcededPerYear,
    filteredTop10EconomicalBowlers2015,
};